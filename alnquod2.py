#!/usr/bin/env python3
import os
import numpy as np
os.environ['MPLBACKEND'] = 'qt5agg'

import quod

class FragmentHydropathy(quod.Hydropathy):
	def __init__(self, fragseq, fullseq, style='r', offset=0, window=19, index=quod.HYDROPATHY, flanksize=None, kernel=None):
		self.style = style
		self.linewidth = 1.0
		self.linestyle = '-'
		self.marker = ''
		flanksize = int(np.ceil(window/2)) if flanksize is None else flanksize

		quod.debug(fragseq, fullseq)


		fragseq[fragseq.find('\n'):]

		rawhydro = []
		for c in fragseq[fragseq.find('\n'):]: 
			if not c.strip(): continue
			rawhydro.append(index.get(c, quod.np.nan))

		#self.Y = rawhydro
		self.X = []
		self.Y = []
		kernel = quod.get_kernel('simple', 19)

		#self.Y = quod.np.convolve(rawhydro, kernel, 'valid')
		self.Y = [sum(rawhydro[i:i+window])/window for i in range(len(rawhydro)-window+1)]
		
		for i in range(len(self.Y)): self.X.append(i+window//2)


	def draw(self, plot):
		#super(FragmentHydropathy, self).draw(plot)
		plot.ax.plot(self.X, self.Y)


if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('frag')
	parser.add_argument('full')
	args = parser.parse_args()

	fragseq = open(args.frag).read()
	fullseq = open(args.full).read()
	hydro = FragmentHydropathy(fragseq, fullseq)

	fig = quod.plt.figure()
	ax = fig.add_subplot(111)

	plot = quod.Plot(fig=fig, ax=ax)
	plot.add(hydro)
	plot.render()

	fig.canvas.draw()

	quod.plt.show()
