# Documentation for script: _multiquod.py_

## Summary
This script is intended to produce publication-quality figures involving plotted sequence information (e.g. hydropathy, compositional entropy, conformational entropy, average hydropathy/amphipathicity/similarity, and others).
It takes config files\* written in a shell-like syntax as input and produces figures of arbitrary size and resolution.

\* Multiquod now supports loading multiple files.
This is functionally equivalent to `cat`ting the files together.

## Dependencies
The following programs need to be available in your path for this program to run properly:

1. **_Python 2.7+_**  
Visit the [official website](https://www.python.org/). 
This program was not tested with more recent versions of Python but was implemented with some forward compatibility.

2. **_Matplotlib 1.5.1-2.0.0+_**  
Visit the [official website](https://matplotlib.org/).

3. **Avehas3** *(optional)*
Clone from [the repository](https://gitlab.com/khendarg/avehas3).

## Terminology

### Color specification

Color specifications are parsed
 - Color specification: If an integer, pulls from the typical cycling colors used for QUOD plots (0=red/orange, 1=blue/cyan).
If an HTML color name (or a Matplotlib color name) pulls the 
 - Subplot IDs: Defined by `def` directives. Must be unique.

## Directives

### **add**

The add directive governs the addition of plot entities other than curves or TMS spans.

#### **add domain**

Usage:
```
add domain SUBPLOTID LEFT RIGHT BOTTOM TOP COLOR TEXT [FONTSIZE] [VALIGN] [HALIGN]
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `LEFT, RIGHT, BOTTOM, TOP`: Rectangle edges (x, x, y, and y)
 - `COLOR`: See [Terminology](#terminology)
 - `TEXT`: Text to place on domain.
 - `FONTSIZE`: Font size in points. Defaults to 8.
 - `VALIGN`: Vertical alignment. Defaults to "above"
 - `HALIGN`: Defaults to "center"

This directive adds colored rectangles labeled with text to depict regions in protein sequences.

#### **add walls**

Usage:
```
add walls SUBPLOTID LEFT RIGHT [Y] [YSTRETCH] [SCALE]
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `LEFT, RIGHT`: Wall edges. (x and x)
 - `Y`: Y position of wedge. Defaults to 2.
 - `YSTRETCH`: +, -, or 0. Where to draw the wall edges with respect to the X axis. Defaults to 0 (stretch through entire subplot)
 - `SCALE`: Scale multiplier for wedges. Defaults to 1.0.

This directive adds black bars with inward-pointing wedges surrounding a provided interval.
This is mostly useful for delineating aligned regions in single proteins.

### **addrow**

Usage:
```
addrow SUBPLOTID1 [SUBPLOTID2 [SUBPLOTID3...]]
```
 - `SUBPLOTIDN`: See [Terminology](#terminology)
`addrow` creates rows containing the subplots specified top to bottom and left to right.

### **def**

Usage:
```
def [--frag] [--mode MODE] [--amphi] SUBPLOTID INFILE1 [INFILE2 [INFILE3...]]
```
 - `--frag`: Interpret input sequences as fragment-full sequence pairs in order to get rid of the half-window blanks on either terminus. 
The total number of sequences contained in the INFILEs must be even.
 - `--mode`: Set a different mode (e.g. "avehas" or "empty") for this subplot. Defaults to "hydropathy". When using `--mode empty`, the first argument is interpreted as the intended length of the spacer subplot in question.
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `INFILEN`: Filenames. If prefixed with "asis:", interprets arguments as raw sequences.

The `def` directive defines the major contents of subplots.

### **set**

#### **set color**

##### **set color.[subentity]**

#### **set dpi**

Usage:
```
set dpi DPI
```
 - `DPI`: Resolution in dpi

This directive is only relevant when writing to bitmaps.

#### **set font**

Usage:
```
set font SUBPLOTID ATTRIBUTE FONTSIZE
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `ATTRIBUTE`: Subplot attribute to set font size for. Valid options are 
"ctitle",
"ltitle",
"rtitle",
"title",
"xaxis ",
"xticks",
"yaxis",
and
"yticks",
 - `FONTSIZE`: Font size in points

#### **set margin**

Usage:
```
set margin HEIGHT
```
 - `HEIGHT`: Figure margin in Matplotlib's default figure margin units.

#### **set hgap**

Usage:
```
set hgap HGAP
```
 - `HGAP`: Horizontal gap between plots in Matplotlib's figure width units.

#### **set hres**

Usage:
```
set hres HRES
```
 - `HRES`: Horizontal resolution for the grid layout. Defaults to 100.

This directive provides for changing the number of columns Multiquod works with internally. 
The default of 100 seems to work well enough for most purposes.

#### **set linestyle**

#### **set linewidth**

#### **set margin**

Usage:
```
set margin MARGIN
```
 - `MARGIN`: Figure margin in Matplotlib's default figure margin units.

#### **set title**

Usage:
```
set title SUBPLOTID TITLE
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `TITLE`: Title text

Sets a left-aligned title.

##### **set ltitle**

Sets a left-aligned title.

##### **set ctitle**

Sets a center-aligned title.

##### **set rtitle**

Sets a right-aligned title.

#### **set vgap**

Usage:
```
set vgap VGAP
```
 - `VGAP`: Vertical gap between plots in Matplotlib's figure height units.

#### **set weight**

Usage:
```
set weight SUBPLOTID FACTOR
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `FACTOR`: Factor to multiply subplot length by.

Setting `weight` multiplies the effective length of a subplot by a given value.

#### **set width**

Usage:
```
set width WIDTH
```
 - `WIDTH`: Figure width in Matplotlib's default figure width units.

#### **set xlabel**

Usage:
```
set xlabel SUBPLOTID TEXT [FONTSIZE]
```
 - `SUBPLOTID`: See [Terminology](#terminology). If prefixed with "row:", centers the label on the row rather than on the subplot.
 - `TEXT`: Label text.
 - `FONTSIZE`: Font size. Defaults to Matplotlib's axis label size.

#### **set xlim**

Usage:
```
set xlim SUBPLOTID LEFT RIGHT
```
 - `SUBPLOTID`: See [Terminology](#terminology).
 - `LEFT`, `RIGHT`: Left and right bounds for subplot. Defaults to the smallest interval containing all subplot entities.

#### **set xticks**

Usage:
```
set xticks SUBPLOTID STEP
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `STEP`: Interval between x tick marks.

#### **set ylabel**

Usage:
```
set ylabel SUBPLOTID TEXT [FONTSIZE]
```
 - `SUBPLOTID`: See [Terminology](#terminology).
 - `TEXT`: Label text.
 - `FONTSIZE`: Font size. Defaults to Matplotlib's axis label size.

#### **set ylim**

Usage:
```
set ylim SUBPLOTID BOTTOM TOP
```
 - `SUBPLOTID`: See [Terminology](#terminology).
 - `BOTTOM`, `TOP`: Top and bottom bounds for subplot. Defaults to the smallest interval containing all subplot entities.

#### **set yticks**

Usage:
```
set xticks SUBPLOTID STEP
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `STEP`: Interval between x tick marks.

### **save**

Usage:
```
save FILENAME
```
 - `FILENAME`: Where to save the figure. 
Matplotlib will autodetect the format based on the file extension.

### **tms**

`tms` directives can be used to add, modify, or remove TMSs from subplots.

#### **tms add**

Usage:
```
tms add SUBPLOTID SEQID LEFT1 RIGHT1 [LEFT2 RIGHT2 [LEFT3 RIGHT3...]]
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `SEQID`: See [Terminology](#terminology)
 - `LEFTN, RIGHTN`: TMS coordinates

`tms add` adds TMSs, for what else could such a directive do?

#### **tms erase**

Usage:
```
tms erase SUBPLOTID SEQID ("all" | LEFT1 RIGHT1 [LEFT2 RIGHT2 [LEFT3 RIGHT3...]])
```
 - `SUBPLOTID`: See [Terminology](#terminology)
 - `SEQID`: See [Terminology](#terminology)
 - `TMSIDN`: Which TMSs 
 - `"all"`: Erase all TMSs
 - `LEFTN, RIGHTN`: Coordinates to erase TMSs in.

